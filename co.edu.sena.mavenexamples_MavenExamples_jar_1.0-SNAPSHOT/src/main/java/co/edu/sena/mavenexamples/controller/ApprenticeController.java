/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.mavenexamples.controller;

import co.edu.sena.mavenexamples.model.Apprentice;
import co.edu.sena.mavenexamples.model.LateArrival;
import co.edu.sena.mavenexamples.persistence.DAOFactory;
import co.edu.sena.mavenexamples.view.utils.Constants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.BlockElement;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Gabriel Cardona
 */
public class ApprenticeController {

    public Apprentice findById(Long document) throws Exception {
        if (document == 0) {
            throw new Exception("el documento es obligatorio");

        }
        return DAOFactory.getApprtenticeDAO().findByid(document);
    }

    public List<Apprentice> findAll() throws Exception {
        return DAOFactory.getApprtenticeDAO().findAll();
    }

    public String setMesage() throws Exception {
        String message = "";
        try {
            List<Apprentice> apprentices = findAll();
            message = "<p>Hola querido jaider osorio hoy es un gran dia </p>"
                    + "<table border='1'>"
                    + "<tr><th>Documento</th><th>Nombre</th><th>Programa</th></tr>";
            for (Apprentice apprentice : apprentices) {
                message += "<tr>" + "<td>" + apprentice.getDocument() + "</td>"
                        + "<td>" + apprentice.getFullName() + "</td>"
                        + "<td>" + apprentice.getIdCourse().getCareer() + "</td>"
                        + "</tr>";

            }
            message += "</table>";
        } catch (Exception e) {
            throw e;
        }
        return message;

    }

    public void sedEmail(String to, String subjet) throws Exception {
        Properties props = new Properties();
        props.put("mail.transport.protocol", Constants.PROTOCOL);
        props.put("mail.smtp.host", Constants.HOST);
        props.put("mail.smtp.socketFactory.port", Constants.SOCKET_FACTORY_PORT);
        props.put("mail.smtp.socketFactory.class", Constants.SOCKET_FACTORY);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Constants.PORT);
        props.put("mail.smtp.user", Constants.USER);
        props.put("mail.smtp.password", Constants.PASSWORD);
        props.put("mail.smtp.starttls.enable","true");

        /*javax.mail*/
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        String bodyMessage = setMesage();
        InternetAddress from = new InternetAddress(props.getProperty("mail.smtp.user"));
        message.setFrom(from);
        message.setSubject(subjet);
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        message.setContent(bodyMessage, "text/html; charset=utf-8");
        /*enviar mensaje */

        Transport transport = session.getTransport(Constants.PROTOCOL);
        transport.connect(props.getProperty("mail.smtp.host"), props.getProperty("mail.smtp.user"), props.getProperty("mail.smtp.password"));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }
    
    public void exportExcel(String path, long document) throws Exception {
       Apprentice apprentice = findById(document);
        File file = new File(path);
        Workbook workbook = new XSSFWorkbook();
// creamos la hoja donde pondremos los datos
        Sheet sheet = workbook.createSheet("LlegadasTardes");
// Creamos el estilo para las celdas del encabezado
        CellStyle styleHeader = workbook.createCellStyle();
        styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderLeft(BorderStyle.THIN);
        styleHeader.setBorderRight(BorderStyle.THIN);
        styleHeader.setBorderTop(BorderStyle.THIN);

        String[] titles = {"id", "fecha", "observacion", "documento", "nombre"};
        Row row = sheet.createRow(0);

        for (int i = 0; i < titles.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellStyle(styleHeader);
            cell.setCellValue(titles[i]);
        }
        
        List<LateArrival> lateArrivalCollection = (List<LateArrival>) apprentice.getLateArrivalCollection();
        CreationHelper creationHelper = workbook.getCreationHelper();
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy/mm/dd"));
        for (int i = 0; i < lateArrivalCollection.size(); i++) {
            row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(lateArrivalCollection.get(i).getId());
            Cell dateArrivalCell = row.createCell(1);
            dateArrivalCell.setCellStyle(dateCellStyle);
            dateArrivalCell.setCellValue(lateArrivalCollection.get(i).getDateArrival());
            row.createCell(2).setCellValue(lateArrivalCollection.get(i).getObservations());
            row.createCell(3).setCellValue(lateArrivalCollection.get(i).getDocumentApprentice().getDocument());
            row.createCell(4).setCellValue(lateArrivalCollection.get(i).getDocumentApprentice().getFullName());

        }

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        workbook.write(fileOutputStream);
        workbook.close();
        System.out.println("Archivo creado "+
        file.getAbsoluteFile());
}
    
    public void exportPDFApprentices(String path) throws Exception{
        
        PdfWriter pdfWriter = new PdfWriter(path);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);
        
        Paragraph paragraph = new Paragraph("Reporte de aprendices");
        paragraph.setFontSize(14);
        paragraph.setTextAlignment(TextAlignment.CENTER);
        paragraph.setBold();
        document.add(paragraph);
        document.add(new Paragraph("")); //salto de linea
        
        float [] columnsWidths = {150F, 150F, 150F};
        Table table = new Table(columnsWidths);
        table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph("Document")));
        table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph("Nombre")));
        table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph("Curso")));
        
        List<Apprentice> apprentices = findAll();
        for (Apprentice apprentice : apprentices) {
            table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph(String.valueOf(apprentice.getDocument()))));
            table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph(apprentice.getFullName())));
            table.addCell(new com.itextpdf.layout.element.Cell().add(new Paragraph(apprentice.getIdCourse().getCareer())));
        }
        
        document.add(table);
        document.close();
        pdfWriter.close();
        System.out.println("PDF Creado mi prro");
        
    }
}
