/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.mavenexamples.persistence;

/**
 *
 * @author Gabriel Cardona
 */
public class DAOFactory {
    
    private static IApprtenticeDAO apprtenticeDAO = new ApprenticeDAO();
    private static ILateArrivalDAO lateArrivalDAO = new LateArrivalDAO();
    private static ICourseDAO courseDAO = new CourseDAO();

    public static IApprtenticeDAO getApprtenticeDAO() {
        return apprtenticeDAO;
    }

    public static ILateArrivalDAO getLateArrivalDAO() {
        return lateArrivalDAO;
    }

    public static ICourseDAO getCourseDAO() {
        return courseDAO;
    }
    
    
}
