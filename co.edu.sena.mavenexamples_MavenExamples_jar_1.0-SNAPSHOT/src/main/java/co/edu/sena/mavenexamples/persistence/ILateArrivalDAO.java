/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.mavenexamples.persistence;

import co.edu.sena.mavenexamples.model.ApprenticeCount;
import co.edu.sena.mavenexamples.model.LateArrival;
import java.util.List;

/**
 *
 * @author Gabriel Cardona
 */
public interface ILateArrivalDAO {
    public LateArrival findByid(Integer Id)throws Exception;
    public List<LateArrival> findAll() throws Exception;
    public List<ApprenticeCount> findQuantityArrivals() throws Exception;
}
