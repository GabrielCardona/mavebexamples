/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.mavenexamples.controller;

import co.edu.sena.mavenexamples.model.ApprenticeCount;
import co.edu.sena.mavenexamples.model.Course;
import co.edu.sena.mavenexamples.model.LateArrival;
import co.edu.sena.mavenexamples.persistence.DAOFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Gabriel Cardona
 */
public class LateArrivalController {
    
    public LateArrival findById(Integer id)throws Exception{
        if (id==0) {
            throw  new Exception("el id es obligatorio");
            
        }
    return DAOFactory.getLateArrivalDAO().findByid(id);
    }
    
    public List<LateArrival> findAll()throws Exception{
        return DAOFactory.getLateArrivalDAO().findAll(); 
    }
    
    public List<ApprenticeCount> findQuantityArrivals() throws Exception{
        return DAOFactory.getLateArrivalDAO().findQuantityArrivals();
    }
    
    public void exportExcel(String path) throws Exception {
        List<ApprenticeCount> apprenticeCounts = findQuantityArrivals();
        File file = new File(path);
        Workbook workbook = new XSSFWorkbook();
// creamos la hoja donde pondremos los datos
        Sheet sheet = workbook.createSheet("Faltas");
// Creamos el estilo para las celdas del encabezado
        CellStyle styleHeader = workbook.createCellStyle();
        styleHeader.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeader.setBorderBottom(BorderStyle.THIN);
        styleHeader.setBorderLeft(BorderStyle.THIN);
        styleHeader.setBorderRight(BorderStyle.THIN);
        styleHeader.setBorderTop(BorderStyle.THIN);

        String[] titles = {"Documento", "CantidadDFaltas"};
        Row row = sheet.createRow(0);

        for (int i = 0; i < titles.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellStyle(styleHeader);
            cell.setCellValue(titles[i]);
        }

        for (int i = 0; i < apprenticeCounts.size(); i++) {
            row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue(apprenticeCounts.get(i).getDocument());
            row.createCell(1).setCellValue(apprenticeCounts.get(i).getCount());

        }

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        workbook.write(fileOutputStream);
        workbook.close();
        System.out.println("Archivo creado "+
        file.getAbsoluteFile());
}
    
}
